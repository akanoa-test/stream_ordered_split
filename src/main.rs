use futures::stream::BoxStream;
use futures::TryStreamExt;
use futures::{future, Stream};
use futures::{stream, StreamExt};
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

fn get_prefix(key: &str) -> Result<String, ()> {
    if !key.starts_with("prefix") {
        return Err(());
    }

    let key = key.split('_').take(2).collect::<Vec<&str>>().join("_");
    Ok(key)
}

fn main() {}

struct Splitter<'a> {
    prefix: Option<String>,
    stream: BoxStream<'a, (String, String)>,
    accumulator: Vec<String>,
    ended: bool,
}

impl<'a> Splitter<'a> {
    #[allow(unused)]
    fn new<S: Stream<Item = (String, String)> + Send + 'a>(stream: S) -> Self {
        Splitter {
            prefix: None,
            stream: Box::pin(stream),
            accumulator: vec![],
            ended: false,
        }
    }
}

impl<'a> Stream for Splitter<'a> {
    type Item = Option<Result<Vec<String>, ()>>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        if self.ended {
            return Poll::Ready(None);
        }

        match Pin::new(&mut self.stream).poll_next(cx) {
            Poll::Ready(result) => match result {
                None => {
                    // release last
                    if !self.accumulator.is_empty() {
                        let data = self.accumulator.drain(..).collect::<Vec<String>>();
                        self.ended = true;
                        return Poll::Ready(Some(Some(Ok(data))));
                    }

                    Poll::Ready(None)
                }
                Some((key, value)) => {
                    // fallible
                    let key = get_prefix(&key);

                    let key = match key {
                        Err(err) => return Poll::Ready(Some(Some(Err(err)))),
                        Ok(key) => key,
                    };

                    // init prefix key
                    if self.prefix.is_none() {
                        self.prefix = Some(key.to_string())
                    }

                    let mut ready_data = vec![];

                    if let Some(ref prefix) = self.prefix {
                        // key has changed
                        if &key != prefix {
                            ready_data = self.accumulator.drain(..).collect::<Vec<String>>();

                            self.prefix = Some(key);
                        }
                    }
                    self.accumulator.push(value.to_string());

                    if ready_data.is_empty() {
                        Poll::Ready(Some(None))
                    } else {
                        Poll::Ready(Some(Some(Ok(ready_data))))
                    }
                }
            },
            Poll::Pending => Poll::Pending,
        }
    }
}

async fn get_data(
    data: Vec<(&str, &str)>,
    skip: Option<usize>,
    limit: Option<usize>,
) -> Result<Vec<String>, ()> {
    let arc_data = Arc::new(data);

    let stream = stream::unfold((0, arc_data), |(index, arc_data)| {
        let result = if let Some(data) = arc_data.get(index) {
            let (x, y) = (data.0.to_string(), data.1.to_string());
            let yielded = (x, y);
            let next_index = index + 1;
            Some((yielded, (next_index, arc_data)))
        } else {
            None
        };
        future::ready(result)
    });

    //let stream = stream::iter(data);
    let splitter = Splitter::new(stream);

    let result_stream = splitter
        .filter_map(future::ready)
        .map(|x: Result<Vec<String>, ()>| x.map(|data| data.join(" ")));

    let data = match (limit, skip) {
        (None, None) => result_stream.try_collect::<Vec<String>>().await?,
        (Some(limit), None) => {
            result_stream
                .take(limit)
                .try_collect::<Vec<String>>()
                .await?
        }
        (None, Some(skip)) => {
            result_stream
                .skip(skip)
                .try_collect::<Vec<String>>()
                .await?
        }
        (Some(limit), Some(skip)) => {
            result_stream
                .skip(skip)
                .take(limit)
                .try_collect::<Vec<String>>()
                .await?
        }
    };

    Ok(data)
}

#[cfg(test)]
mod tests {
    use crate::get_data;

    #[tokio::test]
    async fn test_no_limit_no_skip() {
        let data = vec![
            ("prefix_1_data1", "je"),
            ("prefix_1_data2", "suis"),
            ("prefix_1_data3", "noa"),
            ("prefix_2_data1", "le"),
            ("prefix_2_data2", "ciel"),
            ("prefix_2_data3", "est"),
            ("prefix_2_data4", "gris"),
            ("prefix_3_data1", "Je"),
            ("prefix_3_data2", "m'appelle"),
            ("prefix_3_data3", "pas"),
            ("prefix_3_data4", "Marlin"),
        ];

        let expected = vec![
            "je suis noa".to_string(),
            "le ciel est gris".to_string(),
            "Je m'appelle pas Marlin".to_string(),
        ];

        assert_eq!(Ok(expected), get_data(data, None, None).await);
    }

    #[tokio::test]
    async fn test_with_limit_no_skip() {
        let data = vec![
            ("prefix_1_data1", "je"),
            ("prefix_1_data2", "suis"),
            ("prefix_1_data3", "noa"),
            ("prefix_2_data1", "le"),
            ("prefix_2_data2", "ciel"),
            ("prefix_2_data3", "est"),
            ("prefix_2_data4", "gris"),
            ("prefix_3_data1", "Je"),
            ("prefix_3_data2", "m'appelle"),
            ("prefix_3_data3", "pas"),
            ("prefix_3_data4", "Marlin"),
        ];

        let expected = vec!["je suis noa".to_string(), "le ciel est gris".to_string()];

        assert_eq!(Ok(expected), get_data(data, None, Some(2)).await);
    }

    #[tokio::test]
    async fn test_with_error() {
        let data = vec![
            ("prefix_1_data1", "je"),
            ("prefix_1_data2", "suis"),
            ("prefix_1_data3", "noa"),
            ("prefix_2_data1", "le"),
            ("prefix_2_data2", "ciel"),
            ("faulty", "defsdfef"), // faulty key
            ("prefix_2_data3", "est"),
            ("prefix_2_data4", "gris"),
            ("prefix_3_data1", "Je"),
            ("prefix_3_data2", "m'appelle"),
            ("prefix_3_data3", "pas"),
            ("prefix_3_data4", "Marlin"),
        ];

        assert_eq!(Err(()), get_data(data, None, None).await);
    }

    #[tokio::test]
    async fn test_no_limit_with_skip() {
        let data = vec![
            ("prefix_1_data1", "je"),
            ("prefix_1_data2", "suis"),
            ("prefix_1_data3", "noa"),
            ("prefix_2_data1", "le"),
            ("prefix_2_data2", "ciel"),
            ("prefix_2_data3", "est"),
            ("prefix_2_data4", "gris"),
            ("prefix_3_data1", "Je"),
            ("prefix_3_data2", "m'appelle"),
            ("prefix_3_data3", "pas"),
            ("prefix_3_data4", "Marlin"),
        ];

        let expected = vec![
            "le ciel est gris".to_string(),
            "Je m'appelle pas Marlin".to_string(),
        ];

        assert_eq!(Ok(expected), get_data(data, Some(1), None).await);
    }

    #[tokio::test]
    async fn test_with_limit_with_skip() {
        let data = vec![
            ("prefix_1_data1", "je"),
            ("prefix_1_data2", "suis"),
            ("prefix_1_data3", "noa"),
            ("prefix_2_data1", "le"),
            ("prefix_2_data2", "ciel"),
            ("prefix_2_data3", "est"),
            ("prefix_2_data4", "gris"),
            ("prefix_3_data1", "Je"),
            ("prefix_3_data2", "m'appelle"),
            ("prefix_3_data3", "pas"),
            ("prefix_3_data4", "Marlin"),
        ];

        let expected = vec!["le ciel est gris".to_string()];

        assert_eq!(Ok(expected), get_data(data, Some(1), Some(1)).await);
    }
}
